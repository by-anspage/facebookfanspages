package com.tradevan.dbAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;



public class ReadFromDB {
	private static final String conurl = "jdbc:edb://10.88.56.6:5444/6279test";
	
	private static final String classForName = "com.edb.Driver";
	
	private static final String user = "enterprisedb";
	
	private static final String pas = "caadm99z";
	
	
	private Connection con = null;
	private PreparedStatement ps = null;
	private WriteToCrawlDB wtcdb = new WriteToCrawlDB();
	
	
	private Calendar cal1 = new GregorianCalendar();
	private Calendar cal2 = new GregorianCalendar();
	
	public ReadFromDB() throws ClassNotFoundException, SQLException {
	  Class.forName(classForName);
	  con = DriverManager.getConnection(conurl, user, pas);
	}
	public void close() throws SQLException {
	    this.con.close();
	  }
	
}