package com.tradevan.operate;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class GetWebData {
	
	public Elements postDate,brandName,links,productDescription,reason,company,recallType;
	public Elements projectRecall,dateOfRecall,retailDistributionlist,projectRecallUrl,retailDistributionlistUrl;
	
	
	public Elements title,context,img;
	
	public void run_01() throws IOException {
		String url = "https://www.fda.gov/Safety/Recalls/default.htm";
		System.out.println(String.format("Fetching %s", url));
		
		Document doc = Jsoup.connect(url).get();
		postDate = Xsoup
				.compile(
						"//table/tbody/tr/td[1]")
				.evaluate(doc).getElements();
		brandName = Xsoup
		.compile(
				"//table/tbody/tr/td[2]")
		.evaluate(doc).getElements();
		
		links = Xsoup
		.compile(
				"//table/tbody/tr/td[2]/a/@href")
		.evaluate(doc).getElements();
		
		productDescription = Xsoup
		.compile(
				"//table/tbody/tr/td[3]")
		.evaluate(doc).getElements();
		
		reason = Xsoup
		.compile(
				"//table/tbody/tr/td[4]")
		.evaluate(doc).getElements();
		
		company = Xsoup
		.compile(
				"//table/tbody/tr/td[5]")
		.evaluate(doc).getElements();
		
		recallType = Xsoup
		.compile(
				"//table/tbody/tr/td[6]")
		.evaluate(doc).getElements();
//		System.out.println(String.format("Date: (%d)", postDate.size()));
//		System.out.println(String.format("Brand Name: (%d)", brandName.size()));
//		System.out.println(String.format("Links: (%d)", links.size()));
//		System.out.println(String.format("Product Description: (%d)", productDescription.size()));
//		System.out.println(String.format("Reason/ Problem: (%d)", reason.size()));
//		System.out.println(String.format("Company: (%d)", company.size()));
//		System.out.println(String.format("Recall Type: (%d)", recallType.size()));
//		for (Element link : links) {
//
//			System.out.println(String.format(" * a: <%s>  (%s)",
//					link.attr("abs:href"), link.text()));
//		}
		for (int i=0; i<postDate.size();i++)
		{
//			System.out.println(String.format(" <%s>   <%s>   <%s>   <%s>   <%s>   <%s>   <%s>",
//					postDate.get(i).text(), brandName.get(i).text(), links.get(i).attr("abs:href")
//					, productDescription.get(i).text(), reason.get(i).text(), company.get(i).text()
//					, recallType.get(i).text()
//			));
//			System.out.println( (recallType.get(i).text()).matches(".*\\b220\\b.*"));
			if( (recallType.get(i).text()).matches(".*\\b220\\b.*") )
			{
				getDetail_01(links.get(i).attr("abs:href"));
			}
		}
		
	}
	
	public void run_02() throws IOException {
		String url = "https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/current-recalls-and-alerts";
		System.out.println(String.format("Fetching %s", url));
		
		Document doc = Jsoup.connect(url).get();
		projectRecall = Xsoup
				.compile(
						"//*[@class='title']/span[@class='title-container']")
				.evaluate(doc).getElements();
		
		dateOfRecall = Xsoup
		.compile(
				"//*[@class='recall-date']")
		.evaluate(doc).getElements();
	
		retailDistributionlist = Xsoup
		.compile(
				"//*[@class='qty-recalled']")
		.evaluate(doc).getElements();

		projectRecallUrl = Xsoup
		.compile(
				"//*[@class='title']/span[@class='title-container']/a")
		.evaluate(doc).getElements();

		retailDistributionlistUrl = Xsoup
		.compile(
				"//*[@class='qty-recalled']//a")
		.evaluate(doc).getElements();
//		
//		productDescription = Xsoup
//		.compile(
//				"//table/tbody/tr/td[3]")
//		.evaluate(doc).getElements();
//		
//		reason = Xsoup
//		.compile(
//				"//table/tbody/tr/td[4]")
//		.evaluate(doc).getElements();
//		
//		company = Xsoup
//		.compile(
//				"//table/tbody/tr/td[5]")
//		.evaluate(doc).getElements();
//		
//		recallType = Xsoup
//		.compile(
//				"//table/tbody/tr/td[6]")
//		.evaluate(doc).getElements();
		System.out.println(String.format("Project Recall: (%d)", projectRecall.size()));
		System.out.println(String.format("Date Of Recall Name: (%d)", dateOfRecall.size()));
		System.out.println(String.format("Retail Distributionlist: (%d)", retailDistributionlist.size()));
		System.out.println(String.format("Product Description: (%d)", projectRecallUrl.size()));
		System.out.println(String.format("Reason/ Problem: (%d)", retailDistributionlistUrl.size()));
//		System.out.println(String.format("Company: (%d)", company.size()));
//		System.out.println(String.format("Recall Type: (%d)", recallType.size()));
//		for (Element link : projectRecall) {
//
//			System.out.println(String.format(" * a: <%s>  (%s)",
//					link.attr("abs:href"), link.text()));
//		}
		for (int i=0; i<projectRecallUrl.size();i++)
		{
//			System.out.println(String.format(" <%s>   <%s>   <%s>  ",
//					projectRecall.get(i).text(), dateOfRecall.get(i).text(), retailDistributionlist.get(i).text()
//			));
			
			
				getDetail_02(projectRecallUrl.get(i).attr("abs:href"));
			
		}
		
	}
	

	public void getDetail_01(String url) throws IOException {
		// TODO Auto-generated method stub
//		System.out.println(url);
		Document doc = Jsoup.connect(url).get();
		
		title = Xsoup
		.compile(
				"//article//h1")
		.evaluate(doc).getElements();
		
		context = Xsoup
		.compile(
				"//div[@class='release-text']")
		.evaluate(doc).getElements();
		
		img = Xsoup
		.compile(
				"//img[@class='img-responsive']")
		.evaluate(doc).getElements();
		
		for (Element link : img)
		{
			System.out.println(String.format("https://www.fda.gov/%s",
					link.attr("abs:src") ));
		}
	}
	
	
	public void getDetail_02(String url) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("[INFO]"+url);
		Document doc = Jsoup.connect(url).get();
		System.out.println("[INFO]1");
		title = Xsoup
		.compile(
				"//h3[@class='recall-title-header']")
		.evaluate(doc).getElements();
		System.out.println("[INFO]2");
		context = Xsoup
		.compile(
				"//div[@class='recall-body-container']")
		.evaluate(doc).getElements();
		System.out.println("[INFO]3");
//		img = Xsoup
//		.compile(
//				"//img[@class='img-responsive']")
//		.evaluate(doc).getElements();
		
		for (Element link : context)
		{
			System.out.println(String.format("(%s)",
					link.text() ));
		}
	}
	public Elements getPostDate() {
		return postDate;
	}
	public void setPostDate(Elements postDate) {
		this.postDate = postDate;
	}
	public Elements getBrandName() {
		return brandName;
	}
	public void setBrandName(Elements brandName) {
		this.brandName = brandName;
	}
	public Elements getLinks() {
		return links;
	}
	public void setLinks(Elements links) {
		this.links = links;
	}
	public Elements getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(Elements productDescription) {
		this.productDescription = productDescription;
	}
	public Elements getReason() {
		return reason;
	}
	public void setReason(Elements reason) {
		this.reason = reason;
	}
	public Elements getCompany() {
		return company;
	}
	public void setCompany(Elements company) {
		this.company = company;
	}

}
