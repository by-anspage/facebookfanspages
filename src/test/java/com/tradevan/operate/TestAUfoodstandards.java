package com.tradevan.operate;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class TestAUfoodstandards {

	public void run() throws IOException {
		// this.pagingCount();
		this.detail("http://www.foodstandards.gov.au/industry/foodrecalls/recalls/Pages/CP-Authentic-Asia-Prawn-Wonton-Ramen-with-Green-Choy-Sum.aspx");
	}

	// ���o�`����
	private void pagingCount() throws IOException {
		String url = "http://www.foodstandards.gov.au/industry/foodrecalls/recalls/Pages/default.aspx";
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();

		String pagingCount = Xsoup
				.compile("//div[@class='paging-count']/text()").evaluate(doc)
				.get();
		pagingCount = pagingCount.substring(pagingCount.lastIndexOf("f") + 1)
				.trim();
		System.out.println(pagingCount);
		this.announcement(Integer.parseInt(pagingCount));
	}

	private void announcement(int pagingCount) throws IOException {

		String url = "http://www.foodstandards.gov.au/industry/foodrecalls/recalls/Pages/default.aspx?page=";

		for (int page = 1; page <= pagingCount; page++) {

			System.out.println(String.format("Fetching %s",
					url + String.valueOf(page)));

			Document doc = Jsoup.connect(url).get();

			Elements es;
			List<String> tables = Xsoup
					.compile(
							"//div[@class='ms-WPBody noindex']/div/div/div/table")
					.evaluate(doc).list();

			for (String table : tables) {
				// System.out.println(table);
				List<String> trs = Xsoup.compile("//tbody/tr")
						.evaluate(Jsoup.parse(table)).list();
				// System.out.println(trs.size());
				for (int i = 0; i < trs.size(); i++) {
					System.out.println(trs.get(i));
					switch (i) {
					case 0: // LINK&DATE
						// LINK
						es = Xsoup.compile("//h3/a")
								.evaluate(Jsoup.parse(trs.get(i)))
								.getElements();
						for (Element e : es) {
							System.out.println(String.format(
									" Link: <%s>  (%s)", e.attr("abs:href"),
									e.text()));

						}

						// DATE
						es = Xsoup.compile("//div")
								.evaluate(Jsoup.parse(trs.get(i)))
								.getElements();
						for (Element e : es) {
							System.out.println(String.format(" Date:   (%s)",
									e.text()));

						}
						break;
					case 1: // COMPANY
						es = Xsoup
								.compile("//td")
								.evaluate(
										Jsoup.parse("<table><tbody>"
												+ trs.get(i)
												+ "</tbody></table>"))
								.getElements();
						for (Element e : es) {
							System.out.println(String.format(
									" Company:   (%s)", e.text()));

						}
						break;
					case 2: // REASON
						es = Xsoup
								.compile("//td")
								.evaluate(
										Jsoup.parse("<table><tbody>"
												+ trs.get(i)
												+ "</tbody></table>"))
								.getElements();
						for (Element e : es) {
							System.out.println(String.format(
									" PRODUCT:   (%s)", e.text()));

						}
						break;
					}
				}

			}
		}
	}

	private void detail(String url) throws IOException {
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();

		// Elements es;

		// PRODUCT
		String product = Xsoup.compile("//h1[@id='MainHeading']/text()")
				.evaluate(doc).get().trim();
		System.out.println(product);

		// PRODUCT_DETAIL
		String detail = "";
		Elements details = Xsoup
				.compile("//h2[@class='ms-rteElement-H2']/text()")
				.evaluate(doc).getElements();
		for (Element d : details) {
			detail += d.text() + " ";
		}
		System.out.println(detail.trim());

		// PROBLEM
		String problem = "";
		Elements problems = Xsoup
				.compile("//p[@class='ms-rteElement-P']/text()").evaluate(doc)
				.getElements();
		for (Element p : problems) {
			problem += p.text() + " ";
		}
		System.out.println(problem.trim());

		// IMG
		Elements imgs = Xsoup
				.compile("//h2[@class='ms-rteElement-H2']/span/span/img/@src")
				.evaluate(doc).getElements();
		for (Element img : imgs) {
			System.out
					.println(String.format("Img: <%s> ", img.attr("abs:src")));
		}

	}

}
