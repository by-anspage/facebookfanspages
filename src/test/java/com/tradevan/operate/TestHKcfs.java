package com.tradevan.operate;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class TestHKcfs {

	public void run() throws IOException {
		// this.announcement("2017", "05");
		this.detail("http://www.cfs.gov.hk/tc_chi/press/20170616_0869.html");
	}

	private void announcement(String year, String month) throws IOException {

		String domain = "http://www.cfs.gov.hk/tc_chi/press/";
		String url = String
				.format("http://www.cfs.gov.hk/tc_chi/press/cfs_press_archive_%s_%s_c.html",
						year, month);
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();
		Elements es;

		List<String> trs = Xsoup
				.compile(
						"//table[@class='content']/tbody/tr[@bgcolor='#D9ECE6']")
				.evaluate(doc).list();

		for (String tr : trs) {
			es = Xsoup
					.compile("//td")
					.evaluate(
							Jsoup.parse("<table><tbody>" + tr
									+ "</tbody></table>")).getElements();
			for (int i = 0; i < es.size(); i++) {
				switch (i) {
				case 0: // DATE
					System.out.println(String.format(" Date:   (%s)", es.get(i)
							.text()));

					break;
				case 1: // PROBLEM & LINK
					System.out.println(String.format(" PROBLEM:   (%s)", es
							.get(i).text())); // PROBLEM
					System.out.println(String.format(" LINK:   (%s)", es.get(i)
							.select("a").first().attr("href"))); // LINK
					break;
				}
			}
		}

	}

	private void detail(String url) throws IOException {
		System.out.println(String.format("Fetching %s", url));

		Document doc = Jsoup.connect(url).get();

		Elements es;
		Element e;

		e = Xsoup.compile("//td[@class='content']/table").evaluate(doc)
				.getElements().first();

		es = e.select("tr");

		for (int i = 0; i < es.size(); i++) {
			switch (i) {
			case 0:
				System.out.println(String.format(" PROBLEM:   (%s)", es.get(i)
						.text())); // PROBLEM
				break;
			case 1:
				System.out.println(String.format(" CONTEXT:   (%s)", es.get(i)
						.text().trim())); // CONTEXT
				break;
			}
		}

		// PROBLEM

	}

}
