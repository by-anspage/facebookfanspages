package com.tradevan;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.mail.EmailException;
import org.junit.Test;

import com.tradevan.dbaccess.GetData;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;
import com.tradevan.util.EmailSender;

import facebook4j.internal.org.json.JSONObject;

public class UnitTests {
	public static void main(String[] args) throws Exception {
		UnitTests t= new UnitTests();
		// GetData data = new GetData();
		// ResultSet rs = data.getDataBySQL("select * from committee_fanspage where id
		// ='5'");
		// while (rs.next()) {
		// int id = rs.getInt("id");
		// String committee = rs.getString("committee");
		// String fanspageid = rs.getString("fanspageid");
		// Timestamp opdts = rs.getTimestamp("opdts");
		// System.out.println("id=" + id + ",committee=" + committee + ",fanspageid=" +
		// fanspageid + ",opdts=" + opdts);
		// }
		// rs.close();

//		Configuration configuration = CeawlerConfig.getConfigs();
//		EmailSender emailSender = new EmailSender();
//		String string = configuration.getString(CrawlerConstant.EMAIL_RECEIVERS_KEY);
//		List<String> receiverList = new ArrayList<String>();
//		String[] split = string.split(",");
//		for (String s : split) {
//			receiverList.add(s.trim());
//		}
//		string = configuration.getString(CrawlerConstant.EMAIL_CC_KEY);
//		List<String> ccList = new ArrayList<String>();
//		split = string.split(",");
//		for (String s : split) {
//			ccList.add(s.trim());
//		}
//		emailSender.sendHtmlMail(configuration.getString(CrawlerConstant.EMAIL_HOST_KEY),
//				configuration.getInt(CrawlerConstant.EMAIL_SMTP_PORT_KEY),
//				configuration.getString(CrawlerConstant.EMAIL_SENDER_KEY), receiverList, ccList, "Test Send Mail.", "test send mail from local");
		
		t.testGetFromFB();
	}

	public void testGetFromFB() throws Exception {
		String pageid = "yumeinu";
		String since = "2018-11-22";
		String endDate = "2018-11-22";
		String accessToken = "EAAKPKKsOsw0BAL3CXYI0BDXwPvcCrgw28IIne7uRbfaqZAra4p21DT5dvHBB0hzivXK2FhyR3XX2NZCxRWvvroa2d8ZAZAsf5aiGjftiFq6ZC0PMkwNzazDygsZCN8aYMSFPWEU8QVelPHb6pax0gqTA0vDjDC1FcZD";
		String url = "https://graph.facebook.com/v3.2/" + pageid
				+ "/feed?fields=message,comments.limit(0).summary(true),shares,likes.summary(true),created_time,from&since="
				+ since + "&until=" + endDate + "&access_token=" + accessToken;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		// add request header
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		JSONObject myResponse = new JSONObject(response.toString());
		System.out.println("response : " + myResponse.toString());
	}

	@Test
	public void testGetDataFromDB() throws ClassNotFoundException, SQLException, ConfigurationException {
		GetData data = new GetData();
		ResultSet dataBySQL = data.getDataBySQL("select * from committee_fanspage where id ='5'");
		System.out.println("123 = " + dataBySQL.toString());
	}
}
