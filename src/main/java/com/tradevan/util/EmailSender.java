package com.tradevan.util;

import java.util.List;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

public class EmailSender {
	private static Logger logger = Logger.getLogger(EmailSender.class);
	
	public void sendHtmlMail(String host, int smtpPort, String sender, List<String> receivers, List<String> ccList, String subject, String msg) throws EmailException {
		logger.info("Start to send mail from " + sender + " to " + receivers.toString());
		HtmlEmail email = new HtmlEmail();
		email.setHostName(host);
		email.setSmtpPort(smtpPort);
//		email.setStartTLSEnabled(true);
		email.setFrom(sender);
		if (receivers != null) {
			for (String receiver : receivers) {
				email.addTo(receiver);
			}
		}
		if (ccList != null) {
			for (String cc : ccList) {
				email.addCc(cc);
			}
		}
		email.setSubject(subject);
		email.setHtmlMsg(msg);
		email.send();
		logger.info("Send mail complete.");
		
	}
}
