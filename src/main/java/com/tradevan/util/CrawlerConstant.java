package com.tradevan.util;

public class CrawlerConstant {
	public static final String DB_ACCESS_HOST_KEY = "db.host";
	public static final String DB_ACCESS_PORT_KEY = "db.port";
	public static final String DB_ACCESS_DATABASE_KEY = "db.database";
	public static final String DB_ACCESS_USERNAME_KEY = "db.username";
	public static final String DB_ACCESS_PASSWORD_KEY = "db.password";
	public static final String DB_ACCESS_DRIVER_KEY = "db.driver";
	public static final String DB_ACCESS_DRIVERTYPE_KEY = "db.drivertype";
	public static final String DB_ACCESS_CONN_POOL_MAX_TOTAL_KEY = "db.conn.pool.max.total";
	public static final String DB_ACCESS_CONN_POOL_MAX_IDLE_KEY = "db.conn.pool.max.idle";
	public static final String DB_ACCESS_CONN_POOL_MIN_IDLE_KEY = "db.conn.pool.min.idle";
	public static final String DB_ACCESS_TESTSQL_KEY = "db.testsql";
	
	public static final String EMAIL_HOST_KEY = "email.host";
	public static final String EMAIL_SMTP_PORT_KEY = "email.smtp.port";
	public static final String EMAIL_SENDER_KEY = "email.sender";
	public static final String EMAIL_RECEIVERS_KEY = "email.receivers";
	public static final String EMAIL_CC_KEY = "email.cc";

	public static final String GRAPH_FACEBOOK_VER = "graph.facebook.ver";
}
