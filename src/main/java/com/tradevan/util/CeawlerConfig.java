package com.tradevan.util;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

public class CeawlerConfig {
	private static Configuration configuration = null;
	
	public static Configuration getConfigs() throws ConfigurationException {
		if (configuration == null){
            synchronized(CeawlerConfig.class){
                if(configuration == null) {
                	Configurations configs = new Configurations();
            		configuration = configs.properties("crawler.properties");
                }
            }
		}

		return configuration;
	}
}
