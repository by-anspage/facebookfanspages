package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBprop {

	private String host = "", port = "", database = "", username = "",
			password = "", driver = "", driverType = "";

	public DBprop() {
		FileInputStream fis = null;
		String path = "";
		try {
//			path = this.getClass().getResource("/").getPath();
			// TODO 這邊要抽出去，已經用CeawlerConfig實作了
			path = path + "src/main/resource/crawler.properties";
			fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.indexOf("db.host=") >= 0) {
						this.host = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
					if (read_line.indexOf("db.port=") >= 0) {
						this.port = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
					if (read_line.indexOf("db.database=") >= 0) {
						this.database = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
					if (read_line.indexOf("db.username=") >= 0) {
						this.username = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
					if (read_line.indexOf("db.password=") >= 0) {
						this.password = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
					if (read_line.indexOf("db.driver=") >= 0) {
						this.driver = read_line.substring(
								read_line.indexOf("=") + 1).trim();
					}
					if (read_line.indexOf("db.drivertype=") >= 0) {
						this.driverType = read_line
								.substring(read_line.indexOf("=") + 1)
								.trim();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	public String getDatabase() {
		return database;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getDriver() {
		return driver;
	}

	public String getDriverType() {
		return driverType;
	}

}
