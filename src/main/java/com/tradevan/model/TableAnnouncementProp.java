package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TableAnnouncementProp {

	private String tablename = "", id = "", title = "", company = "",
			brand_name = "", product_description = "", classification = "",
			distribution = "", retail_distribution_list = "",
			retail_distribution_list_url = "", post_date = "",
			source_web_site = "", source_url = "", update_time = "",
			aufs_table= "",	hk_table= "", ca_table = "",
			usdoa_table = "",usfad_table= "";

	public TableAnnouncementProp() {
		FileInputStream fis = null;
		try {
//			TEST
			fis = new FileInputStream(
					"src/main/resource/table_announcement.properties");
//			FORMAL
//			fis = new FileInputStream(
//			"C:/workspace/lib/src/main/resource/table_announcement.properties");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf("tablename:") >= 0) {
						this.tablename = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("id:") >= 0) {
						this.id = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("title:") >= 0) {
						this.title = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("company:") >= 0) {
						this.company = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("brand_name:") >= 0) {
						this.brand_name = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("product_description:") >= 0) {
						this.product_description = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("classification:") >= 0) {
						this.classification = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("distribution:") >= 0) {
						this.distribution = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf(
							"retail_distribution_list:") >= 0) {
						this.retail_distribution_list = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf(
							"retail_distribution_list_url:") >= 0) {
						this.retail_distribution_list_url = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("post_date:") >= 0) {
						this.post_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("source_web_site:") >= 0) {
						this.source_web_site = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("source_url:") >= 0) {
						this.source_url = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("update_time:") >= 0) {
						this.update_time = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("aufs_table:") >= 0) {
						this.aufs_table = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("hk_table:") >= 0) {
						this.hk_table = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("ca_table:") >= 0) {
						this.ca_table = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("usdoa_table:") >= 0) {
						this.usdoa_table = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("usfad_table:") >= 0) {
						this.usfad_table = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getTablename() {
		return tablename;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getCompany() {
		return company;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public String getProduct_description() {
		return product_description;
	}

	public String getClassification() {
		return classification;
	}

	public String getDistribution() {
		return distribution;
	}

	public String getRetail_distribution_list() {
		return retail_distribution_list;
	}

	public String getRetail_distribution_list_url() {
		return retail_distribution_list_url;
	}

	public String getPost_date() {
		return post_date;
	}

	public String getSource_web_site() {
		return source_web_site;
	}

	public String getSource_url() {
		return source_url;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public String getAufs_table() {
		return aufs_table;
	}

	public String getHk_table() {
		return hk_table;
	}

	public String getCa_table() {
		return ca_table;
	}

	public String getUsdoa_table() {
		return usdoa_table;
	}

	public String getUsfad_table() {
		return usfad_table;
	}

}
