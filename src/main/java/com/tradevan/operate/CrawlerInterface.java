package com.tradevan.operate;

import java.util.List;
import java.util.Set;

import com.tradevan.model.CouncilorProp;

public interface CrawlerInterface {
	public void run();

	void parseCouncilor(String webSite);
	
	void parseInfo(String councilorWebSite);

	void parseDetail(CouncilorProp cdata);

	void parseDegree(CouncilorProp cdata);

	void parseCommitte(CouncilorProp cdata);

	void parseExperience(CouncilorProp cdata);
	
	void parseParty(CouncilorProp cdata);

	public String createID();

	void write2Table(String insertSQL, Set<List<String>> setData);

	public boolean isExit(CouncilorProp cdata);
	
	void setBrowser();
	
	public void rendomSleep(int downfrom, int upto);
	
	public String getInfo();

}
