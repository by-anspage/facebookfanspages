package com.tradevan.operate;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogAction {

	protected Logger logger;

	public LogAction() {
//		TEST
//		PropertyConfigurator.configure("src/main/resource/log4j.properties");
//		FORMAL
		PropertyConfigurator.configure("C:/workspace/lib/src/main/resource/log4j.properties");
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogget(Class clazz) {
		this.logger = Logger.getLogger(clazz);

	}
}
