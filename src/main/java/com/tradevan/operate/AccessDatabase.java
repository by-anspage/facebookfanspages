package com.tradevan.operate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.tradevan.bean.ObjCommentReactions;
import com.tradevan.bean.ObjComments;
import com.tradevan.model.CouncilorProp;
import com.tradevan.model.DBprop;
import com.tradevan.model.TableAnnouncementProp;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;

import facebook4j.internal.org.json.JSONException;

public class AccessDatabase extends DBprop {
	private static Logger logger = Logger.getLogger(AccessDatabase.class);

	private static BasicDataSource connectionPool = null;

	// private Connection _con = null;

	// public AccessDatabase(){
	// //TODO 這裡initial 失敗會完全沒人知道
	// try {
	// Class.forName(super.getDriver());
	// this._con = DriverManager.getConnection(String.format(
	// "jdbc:%s://%s/%s?serverTimezone=UTC&characterEncoding=utf8&" +
	// "user=%s&password=%s", super.getDriverType(),
	// super.getHost(), super.getDatabase(), super.getUsername(),
	// super.getPassword()));
	// } catch (ClassNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public AccessDatabase() throws ConfigurationException {
		initConnectionPool();
	}

	public static void initConnectionPool() throws ConfigurationException {
		if (connectionPool == null) {
			synchronized (AccessDatabase.class) {
				if (connectionPool == null) {
					connectionPool = new BasicDataSource();
					Configuration configuration = CeawlerConfig.getConfigs();
					// Class.forName(classForName);
					String driverType = configuration.getString(CrawlerConstant.DB_ACCESS_DRIVERTYPE_KEY);
					String driver = configuration.getString(CrawlerConstant.DB_ACCESS_DRIVER_KEY);
					String host = configuration.getString(CrawlerConstant.DB_ACCESS_HOST_KEY);
					int port = configuration.getInt(CrawlerConstant.DB_ACCESS_PORT_KEY);
					String database = configuration.getString(CrawlerConstant.DB_ACCESS_DATABASE_KEY);
					String username = configuration.getString(CrawlerConstant.DB_ACCESS_USERNAME_KEY);
					String password = configuration.getString(CrawlerConstant.DB_ACCESS_PASSWORD_KEY);
					String testSQL = configuration.getString(CrawlerConstant.DB_ACCESS_TESTSQL_KEY);
					int maxTotal = configuration.getInt(CrawlerConstant.DB_ACCESS_CONN_POOL_MAX_TOTAL_KEY);
					int maxIdle = configuration.getInt(CrawlerConstant.DB_ACCESS_CONN_POOL_MAX_IDLE_KEY);
					int minIdle = configuration.getInt(CrawlerConstant.DB_ACCESS_CONN_POOL_MIN_IDLE_KEY);

					String conUrl = "jdbc:" + driverType + "://" + host + ":" + port + "/" + database
							+ "?serverTimezone=UTC&characterEncoding=utf8";
					connectionPool = new BasicDataSource();
					connectionPool.setUsername(username);
					connectionPool.setPassword(password);
					connectionPool.setDriverClassName(driver);
					connectionPool.setUrl(conUrl);
					connectionPool.setMaxTotal(maxTotal);
					connectionPool.setMaxIdle(maxIdle);
					connectionPool.setMinIdle(minIdle);
					if (!StringUtils.isBlank(testSQL)) {
						connectionPool.setValidationQuery(testSQL);
						connectionPool.setNumTestsPerEvictionRun(maxTotal);
						connectionPool.setTestOnBorrow(true);
						connectionPool.setTestOnReturn(true);
					}
				}
			}
		}
	}

	public static Connection getConnection() throws SQLException, ConfigurationException {
		if (connectionPool == null) {
			initConnectionPool();
		}

		return connectionPool.getConnection();
	}

	// insert data into specific table
	public void write2Table(String insertSQL, Set<List<String>> setData) {
		PreparedStatement ps = null;
		int countBatch = 0;
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			ps = connection.prepareStatement(StringEscapeUtils.unescapeJava(insertSQL));
			int i = 1;
			System.out.println(StringEscapeUtils.unescapeJava(insertSQL));
			for (List<String> datas : setData) {
				countBatch++;
				i = 1;
				for (String s : datas) {
					ps.setString(i++, s);
				}
				ps.addBatch();
				if (countBatch % 100 == 0) {
					ps.executeBatch();
					ps.clearBatch();
				}
			}
			ps.executeBatch();
			ps.clearBatch();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public Set<List<String>> selectFromTable(String selectSQL, List<Integer> indexs) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		Set<List<String>> datas = new HashSet<List<String>>();
		List<String> l = new ArrayList<String>();
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			ps = connection.prepareStatement(StringEscapeUtils.unescapeJava(selectSQL));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				for (int i : indexs) {
					if (rs.getString(i) == null) {
						l.add("");

					} else {
						l.add(rs.getString(i).toString());
					}

				}
			}
			datas.add(l);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return datas;
	}

	public Set<List<String>> selectFromTableWithParameter(String selectSQL, List<String> setData,
			List<Integer> indexs) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		Set<List<String>> datas = new HashSet<List<String>>();
		List<String> l = null;
		// List<String> l = new ArrayList<String>();
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			// Set parameters into selectSQL
			ps = connection.prepareStatement(StringEscapeUtils.unescapeJava(selectSQL));
			int i = 1;
			for (String s : setData) {
				ps.setString(i++, s);
			}
			// Execute the SQL and get response.
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				l = new ArrayList<String>();
				for (int index : indexs) {
					if (rs.getString(index) == null) {
						l.add("");

					} else {
						l.add(rs.getString(index).toString());
					}

				}
				datas.add(l);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return datas;
	}

	public String createID() {
		// TODO Auto-generated method stub
		TableAnnouncementProp announcementProp = new TableAnnouncementProp();
		PreparedStatement ps = null;
		int id = 0;
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			// Set selectSQL
			// ps = this._con
			// .prepareStatement(StringEscapeUtils
			// .unescapeJava("select max(cast(id as int)) as id FROM announcement"));
			ps = connection.prepareStatement(
					StringEscapeUtils.unescapeJava(String.format("select max(cast(%s as int)) as id FROM %s",
							announcementProp.getId(), announcementProp.getTablename())));

			// Execute the SQL and get response.
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				id = rs.getInt("id");

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return String.format("%010d", ++id);
	}

	public boolean isExist(String selectSQL, CouncilorProp a) {

		return false;
	}

	// FACEBOOK FANSPAGE COMMENTS
	public void writePageComments(List<ObjComments> comments, String table) throws SQLException {

		PreparedStatement ps = null, ps_check = null;
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int count = 0;
		boolean isExist = false;

		String sql = "insert into " + table
				+ " (pageid, postid, userid, username, createdtime, messagetag_id,messagetag_name, comment_message ,update_time,shares,likes,comment_count,type) values(?,?,?,?,?,?,?,?,?,?,?,?,?)"
				+ "";
		Connection connection = connectionPool.getConnection();
		ps = connection.prepareStatement(sql);
		logger.debug("comments.size() = " + comments.size());
		for (ObjComments comment : comments) {
			isExist = false;
			if (!isExist) {
				count++;

				ps.setString(1, comment.getPageId());
				ps.setString(2, comment.getPostId());
				ps.setString(3, comment.getUserId());
				ps.setString(4, comment.getUserName());
				ps.setString(5, comment.getCreatedTime());
				ps.setString(6, comment.getMessageTagId());
				ps.setString(7, comment.getMessageTagName());
				ps.setString(8, comment.getMessage());
				ps.setString(9, sdFormat.format(System.currentTimeMillis()).replaceAll(" ", "T") + "+0000");
				ps.setString(10, comment.getShares());
				ps.setString(11, comment.getLikes());
				ps.setString(12, comment.getComment_count());
				ps.setString(13, comment.getType());
				logger.debug("ps = " + ps);
				logger.debug("==========");
				// ps.executeUpdate();
				ps.addBatch();
			}

			if (count % 25 == 0) {
				ps.executeBatch();
				ps.clearBatch();
			}
		}
		ps.executeBatch();
		ps.close();
		connection.close();
	}

	// FACEBOOK FANSPAGE COMMENTS
	public void writePageCommentReactions(List<ObjCommentReactions> comments, String table) throws SQLException {

		PreparedStatement ps = null, ps_check = null;
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int count = 0;
		boolean isExist = false;

		String sql = "insert into " + table
				+ " (pageid, postid, userid, username, createdtime, messagetag_id,messagetag_name, comment_message ,update_time,comment_count,likes,like_count,love,wow,haha,sad,angry,thankful) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
				+ "";
		Connection connection = connectionPool.getConnection();
		ps = connection.prepareStatement(sql);
		logger.debug("comments.size() = " + comments.size());
		for (ObjCommentReactions comment : comments) {
			isExist = false;
			if (!isExist) {
				count++;

				ps.setString(1, comment.getPageId());
				ps.setString(2, comment.getPostId());
				ps.setString(3, comment.getUserId());
				ps.setString(4, comment.getUserName());
				ps.setString(5, comment.getCreatedTime());
				ps.setString(6, comment.getMessageTagId());
				ps.setString(7, comment.getMessageTagName());
				ps.setString(8, comment.getMessage());
				ps.setString(9, sdFormat.format(System.currentTimeMillis()).replaceAll(" ", "T") + "+0000");
				ps.setString(10, comment.getCommentCount());
				ps.setString(11, comment.getLikes());
				ps.setString(12, comment.getTotal_count());
				ps.setString(13, comment.getLove());
				ps.setString(14, comment.getWow());
				ps.setString(15, comment.getHaha());
				ps.setString(16, comment.getSad());
				ps.setString(17, comment.getAngry());
				ps.setString(18, comment.getThanksful());
				logger.debug("ps = " + ps);
				logger.debug("==========");
				// ps.executeUpdate();
				ps.addBatch();
			}

			if (count % 25 == 0) {
				ps.executeBatch();
				ps.clearBatch();
			}
		}
		ps.executeBatch();
		ps.close();
		connection.close();
	}
}
