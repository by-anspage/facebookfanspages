package com.tradevan.schedule;

import java.util.ArrayList;
//import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.tradevan.dbaccess.GetData;
import com.tradevan.model.QueryProp;

public class RunETLStatus {
	private static Logger logger = Logger.getLogger(RunETLStatus.class);

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		QueryProp qp = new QueryProp();
		GetData data = new GetData();
		ArrayList<String> sqls = qp.getExecuteSQL();
		for (String sql : sqls) {
			if (sql.length() > 5) {
				data.execute(sql);
			}
		}
		logger.debug("DONE");
	}
}
