package com.tradevan.schedule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.tradevan.dbaccess.GetData;
import com.tradevan.fanpage.GetCommentsReactions;

public class RunGetCommentsReactions {
	private static Logger logger = Logger.getLogger(RunGetCommentsReactions.class);

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar dateStr = Calendar.getInstance(), dateEnd = Calendar.getInstance();
		dateStr.set(Calendar.DAY_OF_MONTH, dateStr.get(Calendar.DAY_OF_MONTH) - 1);
		String strDate = sdFormat.format(dateStr.getTime());
		dateEnd.set(Calendar.DAY_OF_MONTH, dateEnd.get(Calendar.DAY_OF_MONTH) + 1);
		String endDate = sdFormat.format(dateEnd.getTime());
		String y7date = sdFormat.format(lastNday(7));
		String y14date = sdFormat.format(lastNday(14));
		String y21date = sdFormat.format(lastNday(21));
		String y28date = sdFormat.format(lastNday(28));
		String y30date = sdFormat.format(lastNday(30));
		// 讀取fbTOKEN
		String tokenFilePath = args[0];
		File tokenFile = new File(tokenFilePath);
		String token = "";
		if (!tokenFile.exists()) {
			// TODO 寄出告警信
			throw new Exception("Token file not exist, please check.");
		} else {
			token = FileUtils.readFileToString(tokenFile, "utf8");
		}

		// =================
		// GetCommentsReactions gctr = new
		// GetCommentsReactions("facebook_fanspage_user_comments",
		// "EAAKPKKsOsw0BAL4jmE3qu3PkQuj985p3AZAU4enDpbPvXpQpeKSzZBtuGn58c5v2lfchXkEPZAsZAF2qZAIbEEpi5yVlI8P5OfT1cHWuqg7UyOXMLnBH9BNLFoSrZCCEj9ZBAaLvTsy8cDFGIitFHZCSAdZCitqnwbgZALmuusDtjiQEz0In9waiYA");
		GetCommentsReactions gctr = new GetCommentsReactions("facebook_fanspage_comment_reactions", token);

		GetData data = new GetData();

		data.truncateTable("facebook_fanspage_top10_post");
		data.insertTopTen("facebook_fanspage_user_comments_7days", "7");
		data.insertTopTen("facebook_fanspage_user_comments_14days", "14");
		data.insertTopTen("facebook_fanspage_user_comments_21days", "21");
		data.insertTopTen("facebook_fanspage_user_comments_28days", "28");

		data.deletePast30(y30date);
		List<String> postIdList = data.getTopPostId();
		for (String postid : postIdList) {
			logger.info("Now process postid = " + postid);
			gctr.start(postid, strDate, endDate);
			// TODO 不用睡這麼久了
			int random = 30;
			try {
				Thread.sleep(random * 1000);
			} catch (InterruptedException e) {
			}
			System.out.println("DONE");
		}
		// MyproteinTW
		// String[] fanspageids = {
		// // "237292960686", "130771133668155",
		// "276861822357066", "852926604746233", "191690867518507" };
		// for (String fanspageid : fanspageids) {
		// logger.info("Now process fanspageid = " + fanspageid);
		// gctr.start(fanspageid, strDate, endDate);
		// }
		// 130771133668155
		// 276861822357066
		// 852926604746233
		// 191690867518507

	}

	private static Date lastNday(int N) {
		N = N + 1;
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -N);
		return cal.getTime();
	}
}
