package com.tradevan.schedule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.tradevan.dbaccess.GetData;
import com.tradevan.fanpage.GetComments;
import com.tradevan.model.QueryProp;

public class RunGetCommentsHistory {
	private static Logger logger = Logger.getLogger(RunGetCommentsHistory.class);

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar dateStr = Calendar.getInstance(), dateEnd = Calendar.getInstance();
		dateStr.set(Calendar.DAY_OF_MONTH, dateStr.get(Calendar.DAY_OF_MONTH) - 30);
		String strDate = sdFormat.format(dateStr.getTime());
		dateEnd.set(Calendar.DAY_OF_MONTH, dateEnd.get(Calendar.DAY_OF_MONTH) + 1);
		String endDate = sdFormat.format(dateEnd.getTime());
		
		// 讀取fbTOKEN
		String tokenFilePath = args[0];
		File tokenFile = new File(tokenFilePath);
		String token = "";
		if (!tokenFile.exists()) {
			// TODO 寄出告警信
			throw new Exception("Token file not exist, please check.");
		} else {
			token = FileUtils.readFileToString(tokenFile, "utf8");
		}

		// =================
		// GetComments gct = new GetComments("facebook_fanspage_user_comments",
		// "EAAKPKKsOsw0BAL4jmE3qu3PkQuj985p3AZAU4enDpbPvXpQpeKSzZBtuGn58c5v2lfchXkEPZAsZAF2qZAIbEEpi5yVlI8P5OfT1cHWuqg7UyOXMLnBH9BNLFoSrZCCEj9ZBAaLvTsy8cDFGIitFHZCSAdZCitqnwbgZALmuusDtjiQEz0In9waiYA");
		GetComments gct = new GetComments("facebook_fanspage_user_comments", token);

		// gct.setAccessToken(
		// "EAAKPKKsOsw0BAL3CXYI0BDXwPvcCrgw28IIne7uRbfaqZAra4p21DT5dvHBB0hzivXK2FhyR3XX2NZCxRWvvroa2d8ZAZAsf5aiGjftiFq6ZC0PMkwNzazDygsZCN8aYMSFPWEU8QVelPHb6pax0gqTA0vDjDC1FcZD");

		GetData data = new GetData();
		List<String> fansPageIdList = data.getAllFansPageId();
		for (String fanspageid : fansPageIdList) {
			logger.info("Now process fanspageid = " + fanspageid);
			gct.start(fanspageid, strDate, endDate);
			// TODO 不用睡這麼久了
			int random = 30;
			try {
				Thread.sleep(random * 1000);
			} catch (InterruptedException e) {
			}
			System.out.println("DONE");
		}
		// logger.info(qp.getExecuteSQL());
//		data.execute(qp.getExecuteSQL1());
//		data.execute(qp.getExecuteSQL2());
//		data.execute(qp.getExecuteSQL3());
//		data.execute(qp.getExecuteSQL4());
//		data.execute(qp.getExecuteSQL5());
//		data.execute(qp.getExecuteSQL6());
//		data.execute(qp.getExecuteSQL7());
//		data.execute(qp.getExecuteSQL8());
//		data.execute(qp.getExecuteSQL9());
//		data.execute(qp.getExecuteSQL_10());
//		data.execute(qp.getExecuteSQL_11());
		logger.debug("DONE");
		// MyproteinTW
		// logger.info("Now process fanspageid = " + "MyproteinTW");
		// gct.start("MyproteinTW", strDate, endDate);
	}
}
