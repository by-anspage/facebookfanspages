package com.tradevan.fanpage;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;

import com.tradevan.bean.ObjCommentReactions;
import com.tradevan.bean.ObjComments;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;

import facebook4j.internal.org.json.JSONArray;
import facebook4j.internal.org.json.JSONException;
import facebook4j.internal.org.json.JSONObject;

public class GetCommentsReactions {
	private static Logger logger = Logger.getLogger(GetCommentsReactions.class);

	private String wrtieTable = null;

	private String accessToken = null;

	public GetCommentsReactions(String wrtieTable, String accessToken) {
		this.wrtieTable = wrtieTable;
		this.accessToken = accessToken;
	}

	private AccessDatabase _accesDB = null;

	public void start(String pageid, String since, String endDate) throws Exception {
		if (_accesDB == null) {
			_accesDB = new AccessDatabase();
		}
		try {
			JSONObject json = getResponse(pageid, since, endDate);
			if (json == null) {
				return;
			}
			JSONArray dataArray = json.getJSONArray("data");
			JSONObject object = null;
			List<ObjCommentReactions> ObjCommentsList = new ArrayList<ObjCommentReactions>();
			logger.debug(dataArray.toString());
			for (int i = 0; i < dataArray.length(); i++) {

				logger.debug("Start to process dataArray[" + i + "]");
				object = (JSONObject) dataArray.get(i);
				String postId = object.getString("id");
				ObjCommentsList.add(getCommentData(object, pageid));

				try {
					jsonLoop(object, postId);
				} catch (JSONException e) {
					logger.error("Cant not find comments from " + object.toString());
				} finally {

				}
			}

			_accesDB.writePageCommentReactions(ObjCommentsList, this.wrtieTable);

		} catch (Exception e) {
			throw e;
		}

	}

	private void jsonLoop(JSONObject object, String postId)
			throws JSONException, ClassNotFoundException, SQLException, ConfigurationException {

		String next = null;
		JSONArray commentArray = null;
		JSONObject commentObject = null, eachCommentObject = null;
		List<ObjCommentReactions> ObjCommentsList = new ArrayList<ObjCommentReactions>();

		if (_accesDB == null) {
			_accesDB = new AccessDatabase();
		}
		try {
			commentArray = object.getJSONArray("data");
			logger.debug("[INFO] COMMENT DATA " + commentArray.toString());
		} catch (Exception e) {
			commentArray = object.getJSONArray("data");
			logger.error("[INFO] CAN NOT GET COMMENT " + e);
		}

		try {
			JSONObject commentNextObject = commentObject.getJSONObject("paging");
			next = commentNextObject.get("next").toString();
		} catch (Exception e) {
			logger.debug(" " + commentArray.toString());
		}

		_accesDB.writePageCommentReactions(ObjCommentsList, this.wrtieTable);
		if (next != null) {
			logger.debug("[INFO] NEXT URL " + next);
			jsonLoop(getResponse(next), postId);
		}

	}

	public ObjCommentReactions getCommentData(JSONObject object, String pageId) throws Exception {

		ObjCommentReactions objRections = null;
		JSONObject fromObject = null;
		JSONObject reactionsObject = null;
		JSONObject likesObject = null;
		JSONObject loveObject = null;
		JSONObject wowObject = null;
		JSONObject hahaObject = null;
		JSONObject sadObject = null;
		JSONObject angryObject = null;
		JSONObject thankfulObject = null;
		JSONObject sharesObject = null;

		logger.debug("Start to process dataArray");
		objRections = new ObjCommentReactions();
		objRections.setPageId(pageId);
		String postId = object.getString("id");
		objRections.setPostId(postId);

		try {
			fromObject = object.getJSONObject("from");
		} catch (JSONException e) {
			logger.error("Cant not find shares for postid = " + postId, e);
			fromObject = null;
		}

		if (fromObject != null) {
			String name = null;
			try {
				name = fromObject.getString("name");
			} catch (JSONException e) {
				logger.error("Cant not find from name for postid = " + postId, e);
			} finally {
				objRections.setUserName(name);
			}
			String userId = fromObject.getString("id");
			objRections.setUserId(userId);
		}

		String message = null;
		try {
			message = object.getString("message");
		} catch (JSONException e) {
			logger.error("Cant not find message for postid = " + postId, e);
		} finally {
			objRections.setMessage(message);
		}

		objRections.setCreatedTime(object.get("created_time") == null ? null : object.getString("created_time"));
		// 不需要抓Category，分析有需要會回去對應立委粉絲業的設定
		// objComments.setCategory(category(userId));
		// 2018-11-21 與雨蓉確認 messagetag_name, messagetag_id, comment_id 沒有用到。
		// 情緒用詞
		reactionsObject = object.getJSONObject("reactions");
		objRections.setTotal_count(reactionsObject.getJSONObject("summary").getString("total_count"));

		likesObject = object.getJSONObject("like");
		objRections.setLikes(likesObject.getJSONObject("summary").getString("total_count"));

		loveObject = object.getJSONObject("love");
		objRections.setLove(loveObject.getJSONObject("summary").getString("total_count"));

		wowObject = object.getJSONObject("wow");
		objRections.setWow(wowObject.getJSONObject("summary").getString("total_count"));

		hahaObject = object.getJSONObject("haha");
		objRections.setHaha(hahaObject.getJSONObject("summary").getString("total_count"));

		sadObject = object.getJSONObject("sad");
		objRections.setSad(sadObject.getJSONObject("summary").getString("total_count"));

		angryObject = object.getJSONObject("angry");
		objRections.setAngry(angryObject.getJSONObject("summary").getString("total_count"));

		thankfulObject = object.getJSONObject("thankful");
		objRections.setThanksful(thankfulObject.getJSONObject("summary").getString("total_count"));

		
		String commentCount = null;
		try {
			commentCount = object.getString("comment_count");
		} catch (JSONException e) {
			logger.error("Cant not find shares for postid = " + postId);
		} finally {
			objRections.setCommentCount(commentCount);
		}

		return objRections;
	}

	private JSONObject getResponse(String pageid, String since, String endDate) throws Exception {
		BufferedReader in = null;
		JSONObject jsonResponse = null;
		String reactions = "reactions.limit(0).summary(1)," + "reactions.type(LIKE).limit(0).summary(1).as(like),"
				+ "reactions.type(LOVE).limit(0).summary(1).as(love),"
				+ "reactions.type(WOW).limit(0).summary(1).as(wow),"
				+ "reactions.type(HAHA).limit(0).summary(1).as(haha),"
				+ "reactions.type(SAD).limit(0).summary(1).as(sad),"
				+ "reactions.type(ANGRY).limit(0).summary(1).as(angry),"
				+ "reactions.type(THANKFUL).limit(0).summary(1).as(thankful)";
		// comments.limit(0).summary(true) 無法正常執行
		// String message_data =
		// "message,comments.limit(0).summary(true),shares,likes.summary(true),created_time,from";
		String message_data = "message,comment_count,created_time,from";

		try {
			// 3.2
			Configuration configuration = CeawlerConfig.getConfigs();
			String graphApiVer = configuration.getString(CrawlerConstant.GRAPH_FACEBOOK_VER);
			URL url = new URL("https://graph.facebook.com/" + graphApiVer + "/" + pageid + "/comments?limit=1000&fields="
					+ reactions + "," + message_data + "&filter=toplevel&access_token=" + this.accessToken);
			// URL url = new URL("https://graph.facebook.com/" + graphApiVer + "/" + pageid
			// +
			// "/feed?fields=message,comments.limit(0).summary(true),shares,likes.summary(true),created_time,from&since="
			// + since + "&until=" + endDate + "&access_token=" + this.accessToken);
			logger.info("[FB URL] " + url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			int responseCode = con.getResponseCode();
			if (responseCode == HttpStatus.OK_200) {
				logger.info("getResponse for pageid = " + pageid + " success.");
			} else {
				logger.error(
						"getResponse for pageid = " + pageid + " failed, please check. Http status = " + responseCode);
				if (responseCode == HttpStatus.FORBIDDEN_403) {
					InputStream errorStream = con.getErrorStream();
					String errorString = IOUtils.toString(errorStream, "utf-8");
					JSONObject errJsonResponse = new JSONObject(errorString);
					try {
						if (errJsonResponse.getJSONObject("error").getInt("code") == 4) {
							logger.error("getResponse for pageid = " + pageid
									+ " failed, may be blocked by facebook. please check. Http status = "
									+ responseCode);
							throw new Exception("May been blocked by facebook. please check.");
						}
					} catch (JSONException e) {
					}
					return null;
				} else if (responseCode == HttpStatus.NOT_FOUND_404) {
					logger.error(
							"pageid = " + pageid + " may been deleted, please check. Http status = " + responseCode);
					return null;
				} else {
					return null;
				}
			}
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine = null;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug(response.toString());
			jsonResponse = new JSONObject(response.toString());
		} catch (Exception e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(in);
		}
		return jsonResponse;
	}

	private JSONObject getResponse(String urlpath) throws JSONException {

		// ===Connection===
		URL url;
		String read_line = "";
		urlpath = urlpath.replaceAll("limit=25", "limit=100000");
		try {
			url = new URL(urlpath);
			InputStream in = url.openStream();
			InputStreamReader isr = new InputStreamReader(in, "UTF-8");
			BufferedReader read_web = new BufferedReader(isr);

			read_line = read_web.readLine();

			read_web.close();
			isr.close();
			in.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// ================

		return new JSONObject(read_line);

	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getWrtieTable() {
		return wrtieTable;
	}

	public void setWrtieTable(String wrtieTable) {
		this.wrtieTable = wrtieTable;
	}
}
