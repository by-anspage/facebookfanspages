package com.tradevan.fanpage;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;

import com.tradevan.bean.ObjComments;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.util.CeawlerConfig;
import com.tradevan.util.CrawlerConstant;

import facebook4j.internal.org.json.JSONArray;
import facebook4j.internal.org.json.JSONException;
import facebook4j.internal.org.json.JSONObject;

public class GetComments {
	private static Logger logger = Logger.getLogger(GetComments.class);

	private String wrtieTable = null;

	private String accessToken = null;

	public GetComments(String wrtieTable, String accessToken) {
		this.wrtieTable = wrtieTable;
		this.accessToken = accessToken;
	}

	private AccessDatabase _accesDB = null;

	public void start(String pageid, String since, String endDate) throws Exception {
		if (_accesDB == null) {
			_accesDB = new AccessDatabase();
		}
		try {
			JSONObject json = getResponse(pageid, since, endDate);
			if (json == null) {
				return;
			}
			JSONArray dataArray = json.getJSONArray("data");
			ObjComments objComments = null;
			JSONObject object = null;
			JSONObject fromObject = null;
			JSONObject commentsObject = null;
			JSONObject likesObject = null;
			JSONObject sharesObject = null;
			List<ObjComments> ObjCommentsList = new ArrayList<ObjComments>();
			for (int i = 0; i < dataArray.length(); i++) {
				logger.debug("Start to process dataArray[" + i + "]");
				objComments = new ObjComments();
				object = (JSONObject) dataArray.get(i);
				objComments.setPageId(pageid);
				String postId = object.getString("id");
				objComments.setPostId(postId);

				try {
					fromObject = object.getJSONObject("from");
				} catch (JSONException e) {
					logger.error("Cant not find shares for postid = " + postId, e);
					fromObject = null;
				}

				if (fromObject != null) {
					String name = null;
					try {
						name = fromObject.getString("name");
					} catch (JSONException e) {
						logger.error("Cant not find from name for postid = " + postId, e);
					} finally {
						objComments.setUserName(name);
					}
					String userId = fromObject.getString("id");
					objComments.setUserId(userId);
				}

				String message = null;
				try {
					message = object.getString("message");
				} catch (JSONException e) {
					logger.error("Cant not find message for postid = " + postId, e);
				} finally {
					objComments.setMessage(message);
				}
				
				String type = null;
				try {
					type = object.getString("type");
				} catch (JSONException e) {
					logger.error("Cant not find type for postid = " + postId, e);
				} finally {
					objComments.setType(type);
				}

				objComments
						.setCreatedTime(object.get("created_time") == null ? null : object.getString("created_time"));
				
				commentsObject = object.getJSONObject("comments");
				objComments.setComment_count(commentsObject.getJSONObject("summary").getString("total_count"));
				likesObject = object.getJSONObject("reactions");
				objComments.setLikes(likesObject.getJSONObject("summary").getString("total_count"));
				try {
					sharesObject = object.getJSONObject("shares");
				} catch (JSONException e) {
					logger.error("Cant not find shares for postid = " + postId, e);
				} finally {
					objComments.setShares(sharesObject == null ? null : sharesObject.getString("count"));
				}
				logger.info("POST " + objComments);
				ObjCommentsList.add(objComments);
			}

			_accesDB.writePageComments(ObjCommentsList, this.wrtieTable);

		} catch (Exception e) {
			throw e;
		}

	}

	private JSONObject getResponse(String pageid, String since, String endDate) throws Exception {
		BufferedReader in = null;
		JSONObject jsonResponse = null;
		try {
			// 3.2
			Configuration configuration = CeawlerConfig.getConfigs();
			String graphApiVer = configuration.getString(CrawlerConstant.GRAPH_FACEBOOK_VER);
			// URL url = new URL("https://graph.facebook.com/" + graphApiVer + "/" + pageid
			// + "/feed?fields=posts{comments{" + reactions + "," + message_data + "}," +
			// reactions + ","
			// + message_data + "}&since=" + since + "&until=" + endDate + "&access_token="
			// + this.accessToken);
			URL url = new URL("https://graph.facebook.com/" + graphApiVer + "/" + pageid
					+ "/posts?fields=comments.limit(0).summary(true),type,message,shares,reactions.limit(0).summary(1),created_time,from&limit=100&since="
					+ since + "&until=" + endDate + "&access_token=" + this.accessToken);
			logger.info("[FB URL] " + url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			int responseCode = con.getResponseCode();
			if (responseCode == HttpStatus.OK_200) {
				logger.info("getResponse for pageid = " + pageid + " success.");
			} else {
				logger.error(
						"getResponse for pageid = " + pageid + " failed, please check. Http status = " + responseCode);
				if (responseCode == HttpStatus.FORBIDDEN_403) {
					InputStream errorStream = con.getErrorStream();
					String errorString = IOUtils.toString(errorStream, "utf-8");
					JSONObject errJsonResponse = new JSONObject(errorString);
					try {
						if (errJsonResponse.getJSONObject("error").getInt("code") == 4) {
							logger.error("getResponse for pageid = " + pageid
									+ " failed, may be blocked by facebook. please check. Http status = "
									+ responseCode);
							throw new Exception("May been blocked by facebook. please check.");
						}
					} catch (JSONException e) {
					}
					return null;
				} else if (responseCode == HttpStatus.NOT_FOUND_404) {
					logger.error(
							"pageid = " + pageid + " may been deleted, please check. Http status = " + responseCode);
					return null;
				} else {
					return null;
				}
			}
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine = null;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug(response.toString());
			jsonResponse = new JSONObject(response.toString());
		} catch (Exception e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(in);
		}
		return jsonResponse;
	}

	private String category(String pageid) {
		URL url;
		String read_line = "", result = "";
		try {

			url = new URL(
					"https://graph.facebook.com/v3.1/" + pageid + "?fields=category&access_token=" + this.accessToken);

			InputStream in = url.openStream();
			InputStreamReader isr = new InputStreamReader(in, "UTF-8");
			BufferedReader read_web = new BufferedReader(isr);

			read_line = read_web.readLine();
			result = (new JSONObject(read_line).get("category")).toString();
			// Close
			read_web.close();
			isr.close();
			in.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		// ================

		return result;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getWrtieTable() {
		return wrtieTable;
	}

	public void setWrtieTable(String wrtieTable) {
		this.wrtieTable = wrtieTable;
	}
}
