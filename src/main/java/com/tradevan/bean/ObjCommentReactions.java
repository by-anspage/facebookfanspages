package com.tradevan.bean;

public class ObjCommentReactions {

	private String pageId, postId, userId, userName, createdTime, messageTagId, messageTagName, message, shares, likes,
			total_count, love, wow, sad, angry, thanksful, haha,commentCount;

	public String getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(String commentCount) {
		this.commentCount = commentCount;
	}

	public String getHaha() {
		return haha;
	}

	public void setHaha(String haha) {
		this.haha = haha;
	}

	public String getTotal_count() {
		return total_count;
	}

	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public String getLove() {
		return love;
	}

	public void setLove(String love) {
		this.love = love;
	}

	public String getWow() {
		return wow;
	}

	public void setWow(String wow) {
		this.wow = wow;
	}

	public String getSad() {
		return sad;
	}

	public void setSad(String sad) {
		this.sad = sad;
	}

	public String getAngry() {
		return angry;
	}

	public void setAngry(String angry) {
		this.angry = angry;
	}

	public String getThanksful() {
		return thanksful;
	}

	public void setThanksful(String thinksful) {
		this.thanksful = thanksful;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public String toString() {
		String text = "============";
		text += "\npageId: " + getPageId();
		text += "\npostId: " + getPostId();
		text += "\nuserId: " + getUserId();
		text += "\nuserName: " + getUserName();
		text += "\ncreatedTime: " + getCreatedTime();
		text += "\nmessageTagId: " + getMessageTagId();
		text += "\nmessageTagName: " + getMessageTagName();
		text += "\nmessage: " + getMessage();
		text += "\ncommentCount: " + getCommentCount();
		text += "\n=============";
		return text;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getShares() {
		return shares;
	}

	public void setShares(String shares) {
		this.shares = shares;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageTagId() {
		return messageTagId;
	}

	public void setMessageTagId(String messageTagId) {
		this.messageTagId = messageTagId;
	}

	public String getMessageTagName() {
		return messageTagName;
	}

	public void setMessageTagName(String messageTagName) {
		this.messageTagName = messageTagName;
	}

}
