package com.tradevan.bean;

import java.util.ArrayList;
import java.util.List;

public class ObjPostLikerShare {

  private String pageid, postid, sharecount, created_time;

  private List<String> id = new ArrayList<String>();

  private List<String> name = new ArrayList<String>();

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }

  public String getSharecount() {
    return sharecount;
  }

  public void setSharecount(String sharecount) {
    this.sharecount = sharecount;
  }

  public String getCreated_time() {
    return created_time;
  }

  public void setCreated_time(String created_time) {
    this.created_time = created_time;
  }

  public List<String> getId() {
    return id;
  }

  public void setId(List<String> id) {
    this.id = id;
  }

  public List<String> getName() {
    return name;
  }

  public void setName(List<String> name) {
    this.name = name;
  }

  public void addID(String id) {
    this.id.add(id);
  }

  public void addName(String name) {
    this.name.add(name);
  }

  public String getPageid() {
    return pageid;
  }

  public void setPageid(String pageid) {
    this.pageid = pageid;
  }

}
