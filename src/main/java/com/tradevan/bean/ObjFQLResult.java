package com.tradevan.bean;

import java.util.HashMap;
import java.util.Map;

public class ObjFQLResult {

  public Map<String, FanPageResult> table = new HashMap<String, FanPageResult>();

  public ObjFQLResult() {
  }

  public void addSchema(String schema) {
    this.table.put(schema, new FanPageResult());
  }

  public void addContent(String schema, String content) {
    FanPageResult f=new FanPageResult();
    f.setPagename(content);
    this.table.put(schema, f);
  }
  
  public void addContent(String schema, String content,int number) {
    FanPageResult f=new FanPageResult();
    f.setPagename(content);
    f.setNumber(number);
    this.table.put(schema, f);
  }
  
  public void addContent(String schema, String content,int number,String pageid) {
    FanPageResult f=new FanPageResult();
    f.setPagename(content);
    f.setNumber(number);
    f.setPageid(pageid);
    this.table.put(schema, f);
  }

  public void clear() {
    this.table.clear();
  }

  public FanPageResult getContext(String schema) {
    return this.table.get(schema);
  }
  

}
